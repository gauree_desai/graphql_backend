const { default: axios } = require('axios')
var graphql = require('graphql')

const {GraphQLString, GraphQLInt,GraphQLList,GraphQLObjectType,GraphQLNonNull}=graphql

const companyType= new GraphQLObjectType({
    name:'company',
    fields:{
        id:{type:GraphQLString},
        name:{type:GraphQLString},
        location:{type:GraphQLString}
    }
})


const userType= new GraphQLObjectType({
    name:'user',
    fields:{
        id:{type:GraphQLString},
        firstName:{type:GraphQLString},
        age:{type:GraphQLInt},
        company:{
            type:companyType
        }
    }
})




const RootQuery = new GraphQLObjectType({
      name:'RootQueryType',
      fields:{
          user:{
              type:userType,
              args:{id:{type:GraphQLString}},
              resolve(parentValue,args){
                  console.log(parentValue)
                  return axios.get(`http://localhost:5500/users/${args.id}`)
                  .then(resp=>resp.data)
                 }
                },
         users:{ 
                 type:new GraphQLList(userType),
                 resolve(){
                    return axios.get(`http://localhost:5500/users`)
                  .then(resp=>resp.data)
                 }
              },
        company:{
                type:companyType,
                args:{id:{type:GraphQLString}},
                resolve(parentValue,args){
                    console.log(parentValue)
                    return axios.get(`http://localhost:5500/companies/${args.id}`)
                    .then(resp=>resp.data)
                   }
               }, 
        companies:{ 
                type:new GraphQLList(companyType),
                resolve(){
                   return axios.get(`http://localhost:5500/companies`)
                 .then(resp=>resp.data)
                }
             }          
            }
    })

const MutationQuery=new GraphQLObjectType({
    name:'Mutation',
    fields:{
        addUser:{
            type:userType,
            args:{
                firstName:{type: new GraphQLNonNull(GraphQLString)},
                age:{type: new GraphQLNonNull(GraphQLInt)},
                companyID:{type:GraphQLString}
            },
            resolve(parentValue,{firstName,age}){
              return axios.post(`http://localhost:5500/users`,{firstName,age})
              .then(resp=>resp.data)
            }
        }
    }
})
module.exports= new graphql.GraphQLSchema({ 
                query:RootQuery,
                mutation:MutationQuery

            })
