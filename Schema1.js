var graphql = require('graphql')

const {GraphQLString, GraphQLObjectType}=graphql

const RootQuery = new GraphQLObjectType({
      name:'RootQueryType',
      fields:{
          hello:{
              type:GraphQLString,
              resolve(){
                  return"Hello World!!!"
              }
            },
          country:{
              type:GraphQLString,
              resolve(){
                  return "USA"
              }
           }   
      }
     }
    )


module.exports= new graphql.GraphQLSchema({ query:RootQuery})
