var { graphql, buildSchema, GraphQLSchema } = require('graphql');
 
// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
      type Query { 
        hello: String ,
        age: Int
       
      }`);
 
// The root provides a resolver function for each API endpoint
var root = {
  hello: () => {
    return 'Hello world!';
  },
  age:10
  
};

module.exports= {schema,root}