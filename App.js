var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var schema =require('./Schema2')
var cors =require('cors')

const app=express()

app.use(cors())
app.use('/graphql',graphqlHTTP({
   schema:schema,
   graphiql:true
    
}))



app.listen(7800)

console.log('Running a GraphQL API server at http://localhost:7800/graphql')
